{{-- Summernote --}}
<script>
    $('.summernote').summernote({
      placeholder: 'Write description note ....',
      tabsize: 2,
      height: 150,
      toolbar: [
        ['style', ['style']],
        ['font', ['bold', 'underline', 'clear']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['table', ['table']],
        ['insert', ['link', 'picture', 'video']],
        ['view', ['fullscreen', 'codeview', 'help']]
      ]
    });
  </script>

<script src="assets/backend/vendor/global/global.min.js"></script>
<script src="assets/backend/vendor/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
<script src="assets/backend/js/custom.min.js"></script>
<script src="assets/backend/js/deznav-init.js"></script>
<script src="assets/backend/js/demo.js"></script>
{{-- <script src="assets/backend/js/styleSwitcher.js"></script> --}}



