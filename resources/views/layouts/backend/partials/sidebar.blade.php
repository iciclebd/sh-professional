<div class="deznav">
    <div class="deznav-scroll">
        <div class="main-profile">
            <div class="image-bx">
                <img src="{{ asset(auth()->user()->image ?? 'assets/images/no_image.png') }}" alt="">
                <a href="{{ route('profile') }}"><i class="fa fa-cog" aria-hidden="true"></i></a>
            </div>
            <h5 class="name">{{ auth()->user()->name }}</h5>
            <p class="email">{{ auth()->user()->email }}</p>
        </div>
        <ul class="metismenu" id="menu">
            @can('my-dashboard')
            <li><a class="has-arrow ai-icon" href="{{ route('my-dashboard') }}" aria-expanded="false"><span class="nav-text">Dashboard</span></a></li>
            @endcan
            @role('user')
            {{-- <li><a class="has-arrow ai-icon" href="{{ route('option', auth()->user()->username) }}" aria-expanded="false"><span class="nav-text">Options</span></a></li> --}}
            <li><a class="has-arrow ai-icon" href="{{ route('download-form', auth()->user()->username) }}" aria-expanded="false"><span class="nav-text">Download-form</span></a></li>
            <li><a class="has-arrow ai-icon" href="{{ route('upload-form', auth()->user()->username) }}" aria-expanded="false"><span class="nav-text">Upload-form</span></a></li>
            <li><a class="has-arrow ai-icon" href="{{ route('message', auth()->user()->username) }}" aria-expanded="false"><span class="nav-text">Message</span></a></li>
            {{-- <li><a class="has-arrow ai-icon" href="{{ route('other', auth()->user()->username) }}" aria-expanded="false"><span class="nav-text">other</span></a></li> --}}
            @endcan
            @can('user')
            <li><a class="has-arrow ai-icon" href="{{ route('user') }}" aria-expanded="false"><span class="nav-text">Users</span></a></li>
            @endcan
            @can('document')
            <li><a class="has-arrow ai-icon" href="{{ route('document') }}" aria-expanded="false"><span class="nav-text">Document</span></a></li>
            @endcan
            @can('setting')
            <li><a class="has-arrow ai-icon" href="{{ route('setting') }}" aria-expanded="false"><span class="nav-text">Setting</span></a></li>
            @endcan
        </ul>
        <div class="copyright">
            <p><strong>Software</strong> Developed by</p>
            <p class="fs-12"> <a href="https://iciclecorporation.com/" target="blank">iciclecorporation.com</a> </p>
        </div>
    </div>
</div>
