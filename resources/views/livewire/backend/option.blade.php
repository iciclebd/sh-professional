<div>
    <x-slot name="header">
        <div class="sub-header">
            <div class="d-flex align-items-center flex-wrap mr-auto">
                <h5 class="dashboard_bar">Users</h5>
            </div>
            <div class="d-flex align-items-center">
                <a href="{{ route('dashboard') }}" class="btn btn-xs btn-primary light logout-btn">Dashboard</a>
            </div>
        </div>
    </x-slot>

    <div class="content-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 d-flex justify-content-center">
                    <div class="col-xl-3 col-xxl-4 col-sm-6">
						<div class="card user-card">
							<div class="card-body pb-0">
								<div class="d-flex mb-3 align-items-center">
									<div class="dz-media mr-3">
										<img src="{{ asset(auth()->user()->image) }}" alt="">
									</div>
									<div>
										<h5 class="title"><a href="javascript:void(0);">{{ $user->name }}</a></h5>
										<span class="text-primary">@ {{ $user->username }}</span>
									</div>
								</div>
								<ul class="list-group list-group-flush">
									<li class="list-group-item">
										<span class="mb-0 title">Email</span> :
										<span class="text-black ml-2">{{ $user->email }}</span>
									</li>
									<li class="list-group-item">
										<span class="mb-0 title">Phone</span> :
										<span class="text-black ml-2">{{ $user->phone }}</span>
									</li>
								</ul>
							</div>
						</div>
					</div>
                </div>
                <div class="col-lg-12">
                    <div class="container d-flex justify-content-around">
                        <div class="card mb-3">
                            <img class="card-img-top img-fluid" src="{{ asset('assets/images/download.png') }}" alt="Card image cap">
                            <div class="card-header d-flex justify-content-center">
                                <a href="{{ route('download-form', $user->username) }}" class="btn btn-success">Download Forms <span class="btn-icon-right"><i class="fa fa-download"></i></span></a>
                            </div>
                        </div>
                        <div class="card mb-3">
                            <img class="card-img-top img-fluid" src="{{ asset('assets/images/download.png') }}" alt="Card image cap">
                            <div class="card-header d-flex justify-content-center">
                                <a href="{{ route('upload-form', $user->username) }}" class="btn btn-success">Upload Forms <span class="btn-icon-right"><i class="fa fa-upload"></i></span></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="container d-flex justify-content-around">
                        <div class="card mb-3">
                            <img class="card-img-top img-fluid" src="{{ asset('assets/images/message.png') }}" alt="Card image cap">
                            <div class="card-header d-flex justify-content-center">
                                <a href="{{ route('message', $user->username) }}" class="btn btn-success">Message <span class="btn-icon-right"><i class="fa fa-commenting"></i></span></a>
                            </div>
                        </div>
                        {{-- <div class="card mb-3">
                            <img class="card-img-top img-fluid" src="../../assets/backend/images/card/2.png" alt="Card image cap">
                            <div class="card-header d-flex justify-content-center">
                                <a href="{{ route('other', $user->username) }}" class="btn btn-success">Others <span class="btn-icon-right"><i class="fa fa-asterisk"></i></span></a>
                            </div>
                        </div> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
