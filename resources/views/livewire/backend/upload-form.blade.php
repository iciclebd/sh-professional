<div>
    <x-slot name="header">
        <div class="sub-header">
            <div class="d-flex align-items-center flex-wrap mr-auto">
                <h5 class="dashboard_bar">Upload Documents</h5>
            </div>
            <div class="d-flex align-items-center">
                <a href="{{ route('dashboard') }}" class="btn btn-xs btn-primary light logout-btn">Dashboard</a>
            </div>
        </div>
    </x-slot>
    <div class="content-body" style="min-height: 1110px;">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-6 col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="basic-form">
                                <form wire:submit.prevent="submit" enctype="multipart/form-data">
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label>Title</label>
                                            <input type="text" class="form-control" placeholder="File title" wire:model="title">
                                            @error('title')
                                            <div class="alert alert-danger" role="alert">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Year</label>
                                            <select id="year" class="form-control default-select" wire:model="year">
                                                <option value="">Choose...</option>
                                                @for($option_year = date('Y')-30; $option_year <= date('Y'); $option_year++)
                                                <option value="{{ $option_year }}">{{ $option_year }}</option>
                                                @endfor
                                            </select>
                                            @error('year')
                                            <div class="alert alert-danger" role="alert">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label>Note</label>
                                            <textarea name="" id="" class="form-control" cols="30" rows="10" placeholder="Write a note for this file" wire:model="note"></textarea>
                                            @error('note')
                                            <div class="alert alert-danger" role="alert">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label>File</label>
                                            <input type="file" class="form-control" wire:model="file">
                                            @error('file')
                                            <div class="alert alert-danger" role="alert">
                                                {{ $message }}
                                            </div>
                                            @enderror
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 d-flex justify-content-center">
                    <div class="col-xl-6 col-xxl-6 col-sm-6">
                        <div class="card user-card">
                            <div class="card-body pb-0">
                                <div class="d-flex mb-3 align-items-center">
                                    <div class="dz-media mr-3">
                                        <img src="{{ asset(auth()->user()->image) }}" alt="">
                                    </div>
                                    <div>
                                        <h5 class="title"><a href="javascript:void(0);">{{ $user->name }}</a></h5>
                                        <span class="text-primary">@ {{ $user->username }}</span>
                                    </div>
                                </div>
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item">
                                        <span class="mb-0 title">Email</span> :
                                        <span class="text-black ml-2">{{ $user->email }}</span>
                                    </li>
                                    <li class="list-group-item">
                                        <span class="mb-0 title">Phone</span> :
                                        <span class="text-black ml-2">{{ $user->phone }}</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
