<div>
    <x-slot name="header">
        <div class="sub-header">
            <div class="d-flex align-items-center flex-wrap mr-auto">
                <h5 class="dashboard_bar">Documents</h5>
            </div>
            <div class="d-flex align-items-center">
                <a href="{{ route('dashboard') }}" class="btn btn-xs btn-primary light logout-btn">Dashboard</a>
            </div>
        </div>
    </x-slot>

    <div class="content-body" style="min-height: 1110px;">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal">Add investor</button>
                            <button type="button" class="btn btn-secondary">Secondary</button>
                            <button type="button" class="btn btn-success">Success</button>
                            <button type="button" class="btn btn-danger">Danger</button>
                            <button type="button" class="btn btn-warning">Warning</button>
                            <button type="button" class="btn btn-info">Info</button>
                            <button type="button" class="btn btn-light">Light</button>
                            <button type="button" class="btn btn-dark">Dark</button>

                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-responsive-md">
                                    <thead>
                                        <tr>
                                            <th style="width:50px;">
                                                <div class="custom-control custom-checkbox checkbox-success check-lg mr-3">
                                                    <input type="checkbox" class="custom-control-input" id="checkAll" required="">
                                                    <label class="custom-control-label" for="checkAll"></label>
                                                </div>
                                            </th>
                                            <th><strong>ROLL NO.</strong></th>
                                            <th><strong>NAME</strong></th>
                                            <th><strong>Email</strong></th>
                                            <th><strong>Date</strong></th>
                                            <th><strong>Status</strong></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox checkbox-success check-lg mr-3">
                                                    <input type="checkbox" class="custom-control-input" id="customCheckBox2" required="">
                                                    <label class="custom-control-label" for="customCheckBox2"></label>
                                                </div>
                                            </td>
                                            <td><strong>542</strong></td>
                                            <td>
                                                <div class="d-flex align-items-center"><img src="../../assets/backend/images/avatar/1.jpg" class="rounded-lg mr-2" width="24" alt=""> <span class="w-space-no">Dr. Jackson</span></div>
                                            </td>
                                            <td>example@example.com </td>
                                            <td>01 August 2020</td>
                                            <td>
                                                <div class="d-flex align-items-center"><i class="fa fa-circle text-success mr-1"></i> Successful</div>
                                            </td>
                                            <td>
                                                <div class="d-flex">
                                                    <a href="#" class="btn btn-primary shadow btn-xs sharp mr-1"><i class="fa fa-pencil"></i></a>
                                                    <a href="#" class="btn btn-danger shadow btn-xs sharp"><i class="fa fa-trash"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox checkbox-success check-lg mr-3">
                                                    <input type="checkbox" class="custom-control-input" id="customCheckBox3" required="">
                                                    <label class="custom-control-label" for="customCheckBox3"></label>
                                                </div>
                                            </td>
                                            <td><strong>542</strong></td>
                                            <td>
                                                <div class="d-flex align-items-center"><img src="../../assets/backend/images/avatar/2.jpg" class="rounded-lg mr-2" width="24" alt=""> <span class="w-space-no">Dr. Jackson</span></div>
                                            </td>
                                            <td>example@example.com </td>
                                            <td>01 August 2020</td>
                                            <td>
                                                <div class="d-flex align-items-center"><i class="fa fa-circle text-danger mr-1"></i> Canceled</div>
                                            </td>
                                            <td>
                                                <div class="d-flex">
                                                    <a href="#" class="btn btn-primary shadow btn-xs sharp mr-1"><i class="fa fa-pencil"></i></a>
                                                    <a href="#" class="btn btn-danger shadow btn-xs sharp"><i class="fa fa-trash"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox checkbox-success check-lg mr-3">
                                                    <input type="checkbox" class="custom-control-input" id="customCheckBox4" required="">
                                                    <label class="custom-control-label" for="customCheckBox4"></label>
                                                </div>
                                            </td>
                                            <td><strong>542</strong></td>
                                            <td>
                                                <div class="d-flex align-items-center"><img src="../../assets/backend/images/avatar/3.jpg" class="rounded-lg mr-2" width="24" alt=""> <span class="w-space-no">Dr. Jackson</span></div>
                                            </td>
                                            <td>example@example.com </td>
                                            <td>01 August 2020</td>
                                            <td>
                                                <div class="d-flex align-items-center"><i class="fa fa-circle text-warning mr-1"></i> Pending</div>
                                            </td>
                                            <td>
                                                <div class="d-flex">
                                                    <a href="#" class="btn btn-primary shadow btn-xs sharp mr-1"><i class="fa fa-pencil"></i></a>
                                                    <a href="#" class="btn btn-danger shadow btn-xs sharp"><i class="fa fa-trash"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Large modal -->
    <div wire:ignore.self class="modal fade bd-example-modal-lg" id="modal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="card">
                    <div class="modal-header">
                        <h5 class="modal-title">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal"><span>×</span></button>
                    </div>
                    <div class="card-body">
                        <form wire:submit.prevent="submit">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>First Name</label>
                                    <input type="text" class="form-control" placeholder="1234 Main St">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Email</label>
                                    <input type="email" class="form-control" placeholder="Email">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Password</label>
                                    <input type="password" class="form-control" placeholder="Password">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>City</label>
                                    <input type="text" class="form-control">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>State</label>
                                    <div class="dropdown bootstrap-select form-control default-select"><select id="inputState" class="form-control default-select" tabindex="-98">
                                        <option selected="">Choose...</option>
                                        <option>Option 1</option>
                                        <option>Option 2</option>
                                        <option>Option 3</option>
                                    </select>
                                </div>
                                <button type="submit" class="btn btn-primary form-control col-12 mt-3">SUBMIT</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--.Large modal -->
</div>
