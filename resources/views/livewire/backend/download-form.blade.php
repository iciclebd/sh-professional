<div>
    <x-slot name="header">
        <div class="sub-header">
            <div class="d-flex align-items-center flex-wrap mr-auto">
                <h5 class="dashboard_bar">Documents</h5>
            </div>
            <div class="d-flex align-items-center">
                <a href="{{ route('dashboard') }}" class="btn btn-xs btn-primary light logout-btn">Dashboard</a>
            </div>
        </div>
    </x-slot>

    <div class="content-body" style="min-height: 1110px;">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-responsive-md">
                                    <thead>
                                        <tr>
                                            <th><strong>#.</strong></th>
                                            <th><strong>Title</strong></th>
                                            <th><strong>Year</strong></th>
                                            <th><strong>Note</strong></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($documents as $document)
                                        <tr>
                                            <td><strong>{{ $loop->iteration }}</strong></td>
                                            <td> {{ $document->title }} </td>
                                            <td>{{ $document->year }}</td>
                                            <td>{{ $document->note }}</td>
                                            <td>
                                                <div class="d-flex">
                                                    <a href="{{ $document->file }}" class="btn btn-success shadow btn-xs sharp mr-1" download><i class="fa fa-download"></i></a>
                                                    <button wire:click="delete({{ $document->id }})" onclick="confirm('Are you sure you want to remove ?') || event.stopImmediatePropagation()" class="btn btn-danger shadow btn-xs sharp"><i class="fa fa-trash"></i></button>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
