<div>
    <x-slot name="header">
        <div class="sub-header">
            <div class="d-flex align-items-center flex-wrap mr-auto">
                <h5 class="dashboard_bar">Users</h5>
            </div>
            <div class="d-flex align-items-center">
                <a href="{{ route('dashboard') }}" class="btn btn-xs btn-primary light logout-btn">Dashboard</a>
            </div>
        </div>
    </x-slot>

    <div class="content-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body d-flex justify-content-between">
                            <button type="button" class="btn btn-primary col-3" data-toggle="modal" data-target="#modal" wire:click="create">Add new user</button>
                            <input type="text" class="form-control form-control-lg col-8" placeholder="Search by name, email, phone" wire:model="search_value">
                        </div>
                    </div>
                </div>
                <div class="col-xl-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-responsive-md table-hover">
                                    <thead>
                                        <tr>
                                            <th><strong>{{ __('NO.') }}</strong></th>
                                            <th><strong>{{ __('Status') }}</strong></th>
                                            <th><strong>{{ __('Name') }}</strong></th>
                                            <th><strong>{{ __('Email') }}</strong></th>
                                            <th><strong>{{ __('Phone') }}</strong></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($users as $user)
                                        <tr>
                                            <td><strong>{{ $loop->iteration }}</strong></td>
                                            <td>
                                                @if ($user->status)
                                                <div class="d-flex align-items-center"><i class="fa fa-circle text-success mr-1"></i>
                                                    {{ __('Active') }} </div>
                                                @else
                                                <div class="d-flex align-items-center"><i class="fa fa-circle text-danger mr-1"></i>{{ __('Inactive') }}
                                                </div>
                                                @endif
                                            </td>
                                            <td>
                                                <div class="d-flex align-items-center"><img src="{{ asset($user->image ?? 'assets/images/no_image.png') }}" class="rounded-lg mr-2" width="24" alt=""> <span class="w-space-no">{{ $user->name }}</span></div>
                                            </td>
                                            <td>{{ $user->email }}</td>
                                            <td>{{ $user->phone }}</td>
                                            <td>
                                                <div class="d-flex">
                                                    <a href="{{ route('option', $user->username) }}" class="btn btn-primary shadow btn-xs sharp mr-1"><i class="fa fa-file-text"></i></a>
                                                    <a href="javascript:void(0)" data-toggle="modal" data-target="#modal" wire:click="select_user_for_edit({{ $user->id }})" class="btn btn-primary shadow btn-xs sharp mr-1"><i class="fa fa-pencil"></i></a>
                                                    <a href="javascript:void(0)" wire:click="select_user_for_delete({{ $user->id }})" data-toggle="modal" data-target="#delete_modal" class="btn btn-danger shadow btn-xs sharp"><i class="fa fa-trash"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Delete modal -->
    <div wire:ignore.self class="modal fade" id="delete_modal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <img src="{{ asset('assets/images/delete-animation.gif') }}" width="200" alt="Delete"> <br>
                    <button type="button" class="btn btn-danger text-white" wire:click="delete" data-dismiss="modal"> Confirm Delete </button>
                    <button type="button" class="btn btn-secondary close-btn" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <!--/. Delete modal -->
    <!-- Large modal -->
    <div wire:ignore.self class="modal fade bd-example-modal-lg" id="modal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="card">
                    <div class="modal-header">
                        <h5 class="modal-title">User information</h5>
                        <button type="button" class="close" data-dismiss="modal"><span>×</span></button>
                    </div>
                    <div class="card-body">
                        <form wire:submit.prevent="save" enctype="multipart/form-data">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" placeholder="Full name" class="form-control" required wire:model="name">
                                @error('name')
                                <div class="alert alert-danger solid alert-square ">
                                    <strong>Error!</strong>
                                    {{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Email</label>
                                    <input type="email" placeholder="Email" class="form-control" required wire:model="email">
                                    @error('email')
                                    <div class="alert alert-danger solid alert-square ">
                                        <strong>Error!</strong>
                                        {{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Phone</label>
                                    <input type="text" placeholder="Phone number" class="form-control" wire:model="phone">
                                    @error('phone')
                                    <div class="alert alert-danger solid alert-square ">
                                        <strong>Error!</strong>
                                        {{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Username</label>
                                    <input type="text" placeholder="User name" name="username" class="form-control" required wire:model="username">
                                    @error('username')
                                    <div class="alert alert-danger solid alert-square ">
                                        <strong>Error!</strong>
                                        {{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Password</label>
                                    <input type="password" placeholder="Password" class="form-control" wire:model="password">
                                    @error('password')
                                    <div class="alert alert-danger solid alert-square ">
                                        <strong>Error!</strong>
                                        {{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Date of birth</label>
                                    <input type="date" class="form-control" wire:model="date_of_birth">
                                    @error('date_of_birth')
                                    <div class="alert alert-danger solid alert-square ">
                                        <strong>Error!</strong>
                                        {{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6 mt-5">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="status" wire:model="status">
                                        <label class="custom-control-label" for="status">
                                            Activate</label>
                                    </div>
                                    @error('status')
                                    <div class="alert alert-danger solid alert-square ">
                                        <strong>Error!</strong>
                                        {{ $message }}</div>
                                    @enderror
                                </div>
                                <button class="btn btn-primary col-md-12" type="submit"> Save </button>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        @if($select_user_for_edit)
                            @foreach ($roles as $role)
                            <div class="col-4">
                                <div class="custom-control custom-checkbox mb-3 checkbox-warning" >
                                    <input type="checkbox" class="custom-control-input" wire:click="roleUpdate('{{ $role->name }}')" @if($select_user_for_edit->hasRole($role->name)) checked @endif id="role-{{ $role->id }}">
                                    <label class="custom-control-label" for="role-{{ $role->id }}">{{ $role->name }}</label>
                                </div>
                            </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--.Large modal -->

</div>
