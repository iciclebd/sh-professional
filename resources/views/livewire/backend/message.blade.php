<div>
    <x-slot name="header">
        <div class="sub-header">
            <div class="d-flex align-items-center flex-wrap mr-auto">
                <h5 class="dashboard_bar">Message</h5>
            </div>
            <div class="d-flex align-items-center">
                <a href="{{ route('dashboard') }}" class="btn btn-xs btn-primary light logout-btn">Dashboard</a>
            </div>
        </div>
    </x-slot>
    <style>
        .message-list-area {
            /* background-color: lightblue; */
            width: 100%;
            height: 600px;
            overflow: auto;
        }
    </style>

    <div class="content-body" style="min-height: 1110px;">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12 col-lg-12">
                    <div class="card">
                        <div class="card-body chat-wrapper p-0">
                            <div class="chat-hamburger">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                            <div class="chart-right-sidebar">
                                <div class="message-bx chat-box">
                                    <div class="d-flex justify-content-between chat-box-header">
                                        <div class="d-flex align-items-center">
                                            <img src="{{ asset($user->image ?? 'assets/images/no_image.png') }}" alt="" class="rounded-circle main-img mr-3">
                                            <h5 class="text-black font-w500 mb-sm-1 mb-0 title-nm">{{ $user->name }}</h5>
                                        </div>
                                        <div class="d-flex align-items-center">

                                        </div>
                                    </div>
                                    <div class="chat-box-area dz-scroll ps" id="chartBox" style="background-image:url('images/chat-bg.png');">
                                        <div data-chat="person2" class="chat active-chat message-list-area" id="message-list-area">
                                            @foreach ($messages as $message)
                                            @if ($message->message_type != auth()->user()->getRoleNames()->first())
                                            <div class="media mb-4 received-msg  justify-content-start align-items-start">
                                                <div class="image-bx mr-sm-3 mr-2">
                                                    <img src="../../assets/backend/images/users/pic2.jpg" alt="" class="rounded-circle img-1">
                                                </div>
                                                <div class="message-received">
                                                    <p class="mb-1">
                                                        {!! $message->message !!}
                                                    </p>
                                                    <span class="fs-12 text-black"> {{ $message->created_at->format('d:m:Y a') }} </span>
                                                </div>
                                            </div>
                                            @else
                                            <div class="media mb-4 justify-content-end align-items-end">
                                                <div class="message-sent">
                                                    <p class="mb-1">
                                                        {!! $message->message !!}
                                                    </p>
                                                    <span class="fs-12 text-black"> {{ $message->created_at->format('d:m:Y a') }}</span>
                                                </div>
                                                <div class="image-bx ml-sm-3 ml-2 mb-4">
                                                    <img src="{{ asset(auth()->user()->image) }}" alt="" class="rounded-circle img-1">
                                                </div>
                                            </div>
                                            @endif
                                            @endforeach
                                        </div>
                                        <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
                                            <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                                        </div>
                                        <div class="ps__rail-y" style="top: 0px; right: 0px;">
                                            <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div>
                                        </div>
                                    </div>
                                    <form wire:submit.prevent="submit" onsubmit="scrool_down()">
                                        <div class="card-footer border-0 type-massage">
                                            <div class="input-group">
                                                <input class="form-control" placeholder="Type message..." onkeypress="scrool_down()" wire:model="message">
                                                <div class="input-group-append">
                                                    {{-- <button type="button" class="btn p-0 mr-3"><i class="las la-paperclip scale5 text-secondary"></i></button> --}}
                                                    {{-- <button type="button" class="btn p-0 mr-3"><i class="las la-image scale5 text-secondary"></i></button> --}}
                                                    <button type="submit" class="send-btn btn-primary btn">SEND</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        var myDiv = document.getElementById("message-list-area");
        myDiv.scrollTop = myDiv.scrollHeight;

        function scrool_down(){
            myDiv.scrollTop = myDiv.scrollHeight;
        }
    </script>
</div>
