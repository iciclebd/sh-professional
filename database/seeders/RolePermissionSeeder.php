<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::create(['name' => 'user']);;
        // Permission::create(['name' => 'my-dashboard']);
        Permission::create(['name' => 'profile']);
        Permission::create(['name' => 'option']);;
        // Permission::create(['name' => 'document']);
        // Permission::create(['name' => 'setting']);
        Permission::create(['name' => 'my-document']);
        Permission::create(['name' => 'download-form']);
        Permission::create(['name' => 'upload-form']);
        Permission::create(['name' => 'message']);
        Permission::create(['name' => 'other']);

        $role = Role::create(['name' => 'admin']);
        $role->syncPermissions(Permission::all());

        $role = Role::create(['name' => 'user']);
        // $role->givePermissionTo('my-dashboard');
        $role->givePermissionTo('profile');
        $role->givePermissionTo('my-document');
        $role->givePermissionTo('message');
        $role->givePermissionTo('option');
        $role->givePermissionTo('download-form');
        $role->givePermissionTo('upload-form');
        $role->givePermissionTo('message');
        $role->givePermissionTo('other');
    }
}
