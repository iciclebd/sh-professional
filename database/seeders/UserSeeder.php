<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->status = true;
        $user->name = 'Md. Sakir Ahmed';
        $user->email = 'admin@gmail.com';
        $user->phone = '01304734623';
        $user->username = 'admin';
        $user->password = bcrypt('password');
        $user->save();
        $user->assignRole('admin');

        $user = new User();
        $user->status = true;
        $user->name = 'Mr. User';
        $user->email = 'user@gmail.com';
        $user->phone = '01304734625';
        $user->username = 'user';
        $user->password = bcrypt('password');
        $user->save();
        $user->assignRole('user');
    }
}
