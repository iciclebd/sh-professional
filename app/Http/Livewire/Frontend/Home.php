<?php

namespace App\Http\Livewire\Frontend;

use App\Http\Requests\Auth\LoginRequest;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Auth;
use Livewire\Component;

class Home extends Component
{
    public $form, $name, $username, $email, $password, $password_confirmation;

    public function login()
    {
        $this->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if (\Auth::attempt(array('email' => $this->email, 'password' => $this->password))) {
            if (Auth::User()->status != true) {
                Auth::logout();
                session()->flash('error', 'Your account is not active.');
            } else {
                session()->flash('message', "You are Login successful.");
                return redirect()->route('dashboard');
            }
        } else {
            session()->flash('error', 'Email and password are wrong.');
        }
    }

    public function registration()
    {
        $this->validate([
            'name'      => 'required|string|min:3',
            'username'  => 'required|string|alpha|unique:users,username',
            'email'     => 'required|email|unique:users,email',
            'password'  => 'required|string|min:6|confirmed'
        ]);

        $user = new User();
        $user->status = true;
        $user->name = $this->name;
        $user->username = $this->username;
        $user->email = $this->email;
        $user->password = bcrypt($this->password);
        $user->save();
        $user->assignRole('user');
        if (\Auth::attempt(array('email' => $this->email, 'password' => $this->password))) {
            if (Auth::User()->status != true) {
                Auth::logout();
                session()->flash('error', 'Successfully created please wait for activation.');
            } else {
                session()->flash('message', "You are Login successful.");
                return redirect()->route('dashboard');
            }
        } else {
            session()->flash('error', 'Email and password are wrong.');
        }
        $this->name = $this->username = $this->email = $this->password = $this->password_confirmation = null;
    }

    public function changeForm($form)
    {
        $this->form = $form;
        $this->name = $this->username = $this->email = $this->password = $this->password_confirmation = null;
    }

    public function mount()
    {
        if (Auth::check()) {
            $this->form = 'dashboard';
        } else {
            $this->form = 'login';
        }
    }

    public function render()
    {
        return view('livewire.frontend.home')->layout('layouts.frontend.app');
    }
}
