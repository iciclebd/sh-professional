<?php

namespace App\Http\Livewire\Backend;

use App\Models\Message as ModelsMessage;
use App\Models\User;
use Auth;
use Livewire\Component;

class Message extends Component
{

    public $user, $messages, $message;

    public function submit()
    {
        // $this->validate([
        //     'message' => 'required|boolean',
        // ]);
        if ($this->message) {
            $message_model = new ModelsMessage();
            $message_model->message_type = Auth::user()->hasRole('admin') ? 'admin' : 'user';
            $message_model->user_id = $this->user->id;
            $message_model->message = $this->message;
            $message_model->save();
            $this->message = null;
            $this->dispatchBrowserEvent('alert', ['type' => 'success',  'message' => 'Message sent']);
        }else{
            $this->dispatchBrowserEvent('alert', ['type' => 'error',  'message' => 'Type your message first']);
        }
    }

    public function mount($username)
    {
        $this->user = Auth::user()->hasRole('admin') ?  User::where('username', $username)->first() : Auth::user();
    }

    public function render()
    {
        $this->messages = $this->user->messages;
        return view('livewire.backend.message')->layout('layouts.backend.app');
    }
}
