<?php

namespace App\Http\Livewire\Backend;

use App\Jobs\EmailSender;
use App\Models\Document;
use App\Models\User;
use App\Notifications\FileUpload;
use Auth;
use Livewire\Component;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Notification;

class UploadForm extends Component
{
    use WithFileUploads;
    public $documents, $user, $title, $year, $note, $file;

    public function submit()
    {
        $this->validate([
            'title' => 'required|string',
            'year' => 'required|string',
            'note' => 'required|string',
            'file' => 'required|file',
        ]);

        try{
            $document = new Document();
            $document->user_id = $this->user->id;
            $document->title = $this->title;
            $document->year = $this->year;
            $document->note = $this->note;
            $document->file = 'storage/'.$this->file->store('files', 'public');
            $document->save();
            $this->title = $this->year = $this->note = $this->file = null;
            $this->dispatchBrowserEvent('alert', ['type' => 'success',  'message' => 'Successfully upload']);
        }catch(\Exception $e){
            $this->dispatchBrowserEvent('alert', ['type' => 'error',  'message' => $e->getMessage()]);
        }

        if(Auth::user()->hasRole('admin')){
            dispatch(new EmailSender($this->user, 'A new file uploaded', 'file-upload', asset($document->file)))->delay(now()->addMinutes(10));
        }else{
            dispatch(new EmailSender(User::role('admin')->get(), 'A new file uploaded by '.$this->user->name, 'file-upload', asset($document->file)));
        }
    }

    public function mount($username)
    {
        // $this->user = User::where('username', $username)->first();
        $this->user = Auth::user()->hasRole('admin') ?  User::where('username', $username)->first() : Auth::user();
    }

    public function render()
    {
        $this->documents = $this->user->documents;
        return view('livewire.backend.upload-form')->layout('layouts.backend.app');
    }
}
