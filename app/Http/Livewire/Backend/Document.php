<?php

namespace App\Http\Livewire\Backend;

use Livewire\Component;

class Document extends Component
{
    public function render()
    {
        return view('livewire.backend.document')->layout('layouts.backend.app');
    }
}
