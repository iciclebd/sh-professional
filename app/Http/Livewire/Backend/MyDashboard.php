<?php

namespace App\Http\Livewire\Backend;

use Livewire\Component;

class MyDashboard extends Component
{
    public function render()
    {
        return view('livewire.backend.my-dashboard')->layout('layouts.backend.app');
    }
}
