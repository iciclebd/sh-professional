<?php

namespace App\Http\Livewire\Backend;

use Livewire\WithFileUploads;
use App\Models\User as ModelsUser;
use Livewire\Component;
use Spatie\Permission\Models\Role;

class User extends Component
{
    use WithFileUploads;

    public $users, $roles, $search_value;
    public $status, $name, $email, $phone, $username, $password, $image, $date_of_birth; // user form
    public $select_user_for_edit, $select_user_for_delete; // user edit and delete

    public function tab_change($new_tab)
    {
        $this->tab = $new_tab;
    }

    public function create()
    {
        $this->status = $this->name = $this->email = $this->phone = $this->username = $this->password = $this->image = $this->blood_goup = $this->date_of_birth = $this->note
        = $this->select_user_for_edit = $this->select_user_for_delete = null;
    }

    // User edit
    public function select_user_for_edit(ModelsUser $user)
    {
        $this->select_user_for_edit = $user;
        $this->status = $user->status;
        $this->name = $user->name;
        $this->email = $user->email;
        $this->phone = $user->phone;
        $this->username = $user->username;
        // $this->password = $user->password;
        // $this->image = $user->image;
        $this->date_of_birth = $user->date_of_birth;
    }

    // User add or edit
    public function save()
    {
        $this->validate([
            'status' => 'nullable|boolean',
            'name' => 'required|string',
            'phone' => 'nullable|string|max:20',
            'image' => 'nullable|image',
            'date_of_birth' => 'nullable|string',
        ]);
        if ($this->select_user_for_edit) {
            $user = $this->select_user_for_edit;
            $this->validate([
                'email' => 'required|email|unique:users,email,' . $user->id,
                'username' => 'required|string|alpha|unique:users,username,' . $user->id,
                'password' => 'nullable|string|min:4,',
            ]);
        } else {
            $this->validate([
                'email' => 'required|email|unique:users,email',
                'username' => 'required|string|alpha|unique:users,username',
                'password' => 'required|string|min:4',
            ]);
            $user = new ModelsUser();
        }
        try {
            $user->status   =  $this->status;
            $user->name     =    $this->name;
            $user->email    =   $this->email;
            $user->phone    =   $this->phone;
            $user->username =    $this->username;
            if ($this->password)
                $user->password =  bcrypt($this->password);
            // $user->image        =  $this->image;
            $user->date_of_birth= $this->date_of_birth;
            $user->save();
            if (!$this->select_user_for_edit){
                $user->assignRole('user');
            }
            $this->create(); //Make input field null
        } catch (\Exception $e) {
            $this->dispatchBrowserEvent('alert', ['type' => 'error',  'message' => $e->getMessage()]);
        }
        $this->dispatchBrowserEvent('alert', ['type' => 'success',  'message' => 'Successfully Done!']);
    }

    public function select_user_for_delete(ModelsUser $user)
    {
        $this->select_user_for_delete = $user;
    }

    public function delete()
    {
        if (auth()->user()->id == $this->select_user_for_delete->id) {
            $this->dispatchBrowserEvent('alert', ['type' => 'error',  'message' => 'Can not delete your self!']);
        } else {
            $this->select_user_for_delete->delete();
            $this->dispatchBrowserEvent('alert', ['type' => 'success',  'message' => 'Successfully Deleted!']);
        }
    }

    public function roleUpdate($role_name){
        if($this->select_user_for_edit){
            if($this->select_user_for_edit->hasRole($role_name)){
                $this->select_user_for_edit->removeRole($role_name);
                $this->dispatchBrowserEvent('alert', ['type' => 'success',  'message' => 'Successfully removed!']);
            }else{
                $this->select_user_for_edit->assignRole($role_name);
                $this->dispatchBrowserEvent('alert', ['type' => 'success',  'message' => 'Successfully assign!']);
            }
        }else{
            $this->dispatchBrowserEvent('alert', ['type' => 'error',  'message' => 'User is not selected for edit!']);
        }
    }

    public function render()
    {
        if($this->search_value){
            $this->users = ModelsUser::where('name', 'LIKE', "%$this->search_value%")
            ->orWhere('email', 'LIKE', "%$this->search_value%")
            ->orWhere('phone', 'LIKE', "%$this->search_value%")
            ->get();
        }else{
            $this->users = ModelsUser::latest()->get();
        }
        $this->roles = Role::latest()->get();
        return view('livewire.backend.user')->layout('layouts.backend.app');
    }
}
