<?php

namespace App\Http\Livewire\Backend;

use Livewire\Component;

class Other extends Component
{
    public function render()
    {
        return view('livewire.backend.other')->layout('layouts.backend.app');
    }
}
