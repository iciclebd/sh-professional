<?php

namespace App\Http\Livewire\Backend;

use App\Jobs\EmailSender;
use App\Models\Document;
use App\Models\User;
use App\Notifications\FileDelete;
use App\Notifications\FileUpload;
use Auth;
use Illuminate\Support\Facades\Notification;
use Livewire\Component;


class DownloadForm extends Component
{
    public $documents, $user;
    public function delete(Document $document)
    {
        if(Auth::user()->hasRole('admin')){
            $document->delete();
            $this->dispatchBrowserEvent('alert', ['type' => 'success',  'message' => 'Successfully deleted']);
            dispatch(new EmailSender($this->user, 'A new file deleted', 'file-delete'));
        }else{
            if($document->user_id == $this->user->id){
                $document->delete();
                $this->dispatchBrowserEvent('alert', ['type' => 'success',  'message' => 'Successfully deleted']);
                dispatch(new EmailSender(User::role('admin')->get(), 'A new file deleted by '.$this->user->name, 'file-delete'));
            }else{
                $this->dispatchBrowserEvent('alert', ['type' => 'error',  'message' => 'Something went wrong']);
            }
        }
    }

    public function mount($username)
    {
        // $this->user = User::where('username', $username)->first();
        $this->user = Auth::user()->hasRole('admin') ?  User::where('username', $username)->first() : Auth::user();
    }

    public function render()
    {
        $this->documents = $this->user->documents;
        return view('livewire.backend.download-form')->layout('layouts.backend.app');
    }
}
