<?php

namespace App\Http\Livewire\Backend;

use App\Models\User;
use Auth;
use Livewire\Component;

class Option extends Component
{
    public $user;

    public function mount($username){
        // $this->user = User::where('username', $username)->first();
        $this->user = Auth::user()->hasRole('admin') ?  User::where('username', $username)->first() : Auth::user();
    }

    public function render()
    {
        return view('livewire.backend.option')->layout('layouts.backend.app');
    }
}
