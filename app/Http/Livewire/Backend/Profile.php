<?php

namespace App\Http\Livewire\Backend;

use Livewire\Component;
use Livewire\WithFileUploads;

class Profile extends Component
{
    use WithFileUploads;
    public $username, $name, $email, $phone, $password, $password_confirmation, $image, $date_of_birth;


    public function save()
    {
        $this->validate([
            'name' => 'required|string',
            'phone' => 'nullable|string|max:20,'.auth()->user()->id,
            'email' => 'required|email|unique:users,email,'.auth()->user()->id,
            'username' => 'required|string|alpha|unique:users,username,'.auth()->user()->id,
            'password' => 'nullable|string|min:4|confirmed',
            'image' => 'nullable|image',
            'date_of_birth' => 'nullable|string',
        ]);

        $user = auth()->user();
        $user->name = $this->name;
        $user->phone = $this->phone;
        $user->email = $this->email;
        $user->username = $this->username;
        if($this->password)
        $user->password = $this->password;
        if($this->image)
        $user->image = 'storage/'.$this->image->store('images', 'public');
        $user->date_of_birth = $this->date_of_birth;
        $user->save();
        $this->dispatchBrowserEvent('alert', ['type' => 'success',  'message' => 'Successfully updated']);
    }

    public function mount()
    {
        $this->username = auth()->user()->username;
        $this->name = auth()->user()->name;
        $this->email = auth()->user()->email;
        $this->phone = auth()->user()->phone;
        // $this->password = auth()->user()->password;
        $this->image = auth()->user()->image;
        $this->date_of_birth = auth()->user()->date_of_birth;
    }

    public function render()
    {
        return view('livewire.backend.profile')->layout('layouts.backend.app');
    }
}
