<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Wildside\Userstamps\Userstamps;

class Document extends Model
{
    use HasFactory, Userstamps;

    protected $fillable = [
        'user_id',
        'title',
        'year',
        'note',
        'file'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
