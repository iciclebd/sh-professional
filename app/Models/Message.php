<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Wildside\Userstamps\Userstamps;

class Message extends Model
{
    use HasFactory, Userstamps;

    protected $fillable = [
        'message_type',
        'user_id',
        'message'
    ];

    public function user()
    {
        return $this->belongTo(User::class, 'user_id', 'id');
    }
}
