<?php

namespace App\Jobs;

use App\Notifications\FileDelete;
use App\Notifications\FileUpload;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class EmailSender implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $user, $message, $type, $file_url;
    public function __construct($user, $message, $type, $file_url = null)
    {
        $this->user = $user;
        $this->message = $message;
        $this->type = $type;
        $this->file_url = $file_url;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if($this->type == 'file-delete'){
            $this->user->notify(new FileDelete($this->message));
        }
        if($this->type == 'file-upload'){
            $this->user->notify(new FileUpload($this->file_url, $this->message));
        }
    }
}
