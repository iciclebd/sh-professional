<?php

use App\Http\Livewire\Backend\Document;
use App\Http\Livewire\Backend\DownloadForm;
use App\Http\Livewire\Backend\Message;
use App\Http\Livewire\Backend\MyDashboard;
use App\Http\Livewire\Backend\Option;
use App\Http\Livewire\Backend\Other;
use App\Http\Livewire\Backend\Profile;
use App\Http\Livewire\Backend\Setting;
use App\Http\Livewire\Backend\UploadForm;
use App\Http\Livewire\Backend\User;
use App\Http\Livewire\Frontend\Home;
use App\Http\Livewire\Frontend\Register;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Frontend route
Route::get('/', Home::class)->name('home');
Route::get('/register', Register::class)->name('register');

// Default redirect
Route::get('/dashboard', function () {
    try{
        return redirect()->route(auth()->user()->getAllPermissions()->first()->name);
    }catch(\Exception $e){
        Auth::guard('web')->logout();
        return redirect()->route('home');
    }
})->middleware(['auth'])->name('dashboard');

// Auth routes
Route::group(['middleware' => ['auth']], function () {
    Route::get('my-dashboard', MyDashboard::class)->name('my-dashboard')->middleware(['permission:my-dashboard']);
    Route::get('profile', Profile::class)->name('profile')->middleware(['permission:profile']);
    Route::get('user', User::class)->name('user')->middleware(['permission:user']);
    Route::get('option/{username}', Option::class)->name('option')->middleware(['permission:option']);
    Route::get('download-form/{username}', DownloadForm::class)->name('download-form')->middleware(['permission:download-form']);
    Route::get('upload-form/{username}', UploadForm::class)->name('upload-form')->middleware(['permission:upload-form']);
    Route::get('message/{username}', Message::class)->name('message')->middleware(['permission:message']);
    Route::get('other/{username}', Other::class)->name('other')->middleware(['permission:other']);
    Route::get('document', Document::class)->name('document')->middleware(['permission:document']);
    Route::get('setting', Setting::class)->name('setting')->middleware(['permission:setting']);
});
